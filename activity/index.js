// S26 Activity


// 5. Import the http module using the required directive.

const http = require('http');


// 6. Create a variable port and assign it with the value of 3000.

const port = 3000;

// 7. Create a server using the createServer method that will listen in to the port provided above.
// 8. Console log in the terminal a message when the server is successfully running.

const server = http.createServer((request, response) => {

// 9. Create a condition that when the login route is accessed, it will print a message to the user that they are in the login page.
// 10. Access the login route to test if it’s working as intended.

	if(request.url == '/login') {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Welcome to the login page.')
	}

// 11. Create a condition for any other routes that will return an error message.
// 12. Access any other route to test if it’s working as intended.


	else if(request.url == '/register'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end(`I'm sorry the page you are looking for cannot be found.`)
	} else {
		response.writeHead(404, {'Content-Type': 'text/plain'})
		response.end('Page not found.')
	}


})


server.listen(port);

console.log(`Server now accessible at localhost:${port}.`);